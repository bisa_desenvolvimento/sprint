<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class MHome extends CI_Model
{	
	public function index()
	{

	}

	public function impedimentos($data) //MANTIS ESTADO IMPEDIMENTO
	{
		$mantis_sprint = $data;

		$sql = "SELECT mantis_project_table.id AS 'Projeto ID', mantis_project_table.name AS 'Projeto Nome', mantis_bug_table.id AS Mantis, mantis_bug_table.reporter_id AS 'Relator ID', relator.username AS 'Relator Usuário', relator.realname AS 'Relator Nome', mantis_bug_table.handler_id AS 'Atribuido a', responsavel.username AS 'Atribuido a Usuário', responsavel.realname AS 'Atribuido a Nome', mantis_bug_table.summary AS 'Resumo', mantis_bug_table.category_id AS 'Categoria', mantis_bug_table.status AS 'Estado', mantis_custom_field_table.name AS 'Campo Nome', mantis_custom_field_string_table.field_id AS 'Campo ID',mantis_custom_field_string_table.value AS 'Campo Conteúdo', (SELECT cf.value FROM mantis_custom_field_string_table cf WHERE cf.field_id = 1 AND cf.bug_id = mantis_bug_table.id) AS 'Cliente' FROM mantis_custom_field_string_table JOIN mantis_bug_relationship_table ON mantis_custom_field_string_table.bug_id = mantis_bug_relationship_table.destination_bug_id JOIN mantis_custom_field_table ON mantis_custom_field_string_table.field_id = mantis_custom_field_table.id JOIN mantis_bug_table ON mantis_bug_table.id = mantis_bug_relationship_table.destination_bug_id JOIN mantis_project_table ON mantis_bug_table.project_id = mantis_project_table.id LEFT JOIN mantis_user_table relator ON relator.id = mantis_bug_table.reporter_id LEFT JOIN mantis_user_table responsavel ON responsavel.id = mantis_bug_table.handler_id WHERE mantis_bug_relationship_table.source_bug_id = ".$mantis_sprint." AND mantis_bug_table.status= 60 GROUP BY Mantis ORDER BY Resumo DESC";

		$query = $this->db->query($sql);

		return $query->result();
	}

	public function sprint($data) { //MANTIS ESTADO NOVO
	

		$mantis_sprint = $data;

		$sql = "SELECT mantis_project_table.id AS 'Projeto ID', mantis_project_table.name AS 'Projeto Nome', mantis_bug_table.id AS Mantis, mantis_bug_table.reporter_id AS 'Relator ID', relator.username AS 'Relator Usuário', relator.realname AS 'Relator Nome', mantis_bug_table.handler_id AS 'Atribuido a', responsavel.username AS 'Atribuido a Usuário', responsavel.realname AS 'Atribuido a Nome', mantis_bug_table.summary AS 'Resumo', mantis_bug_table.category_id AS 'Categoria', mantis_bug_table.status AS 'Estado', mantis_custom_field_table.name AS 'Campo Nome', mantis_custom_field_string_table.field_id AS 'Campo ID',mantis_custom_field_string_table.value AS 'Campo Conteúdo', (SELECT cf.value FROM mantis_custom_field_string_table cf WHERE cf.field_id = 1 AND cf.bug_id = mantis_bug_table.id) AS 'Cliente' FROM mantis_custom_field_string_table JOIN mantis_bug_relationship_table ON mantis_custom_field_string_table.bug_id = mantis_bug_relationship_table.destination_bug_id JOIN mantis_custom_field_table ON mantis_custom_field_string_table.field_id = mantis_custom_field_table.id JOIN mantis_bug_table ON mantis_bug_table.id = mantis_bug_relationship_table.destination_bug_id JOIN mantis_project_table ON mantis_bug_table.project_id = mantis_project_table.id LEFT JOIN mantis_user_table relator ON relator.id = mantis_bug_table.reporter_id LEFT JOIN mantis_user_table responsavel ON responsavel.id = mantis_bug_table.handler_id WHERE mantis_bug_relationship_table.source_bug_id = ".$mantis_sprint." AND mantis_bug_table.status= 10 GROUP BY Mantis ORDER BY Resumo DESC";

		$query = $this->db->query($sql);

		return $query->result();
	}

	public function atribuidos($data) //MANTIS ESTADO ATRIBUÍDO
	{
				$mantis_sprint = $data;

		$sql = "SELECT mantis_project_table.id AS 'Projeto ID', mantis_project_table.name AS 'Projeto Nome', mantis_bug_table.id AS Mantis, mantis_bug_table.reporter_id AS 'Relator ID', relator.username AS 'Relator Usuário', relator.realname AS 'Relator Nome', mantis_bug_table.handler_id AS 'Atribuido a', responsavel.username AS 'Atribuido a Usuário', responsavel.realname AS 'Atribuido a Nome', mantis_bug_table.summary AS 'Resumo', mantis_bug_table.category_id AS 'Categoria', mantis_bug_table.status AS 'Estado', mantis_custom_field_table.name AS 'Campo Nome', mantis_custom_field_string_table.field_id AS 'Campo ID',mantis_custom_field_string_table.value AS 'Campo Conteúdo', (SELECT cf.value FROM mantis_custom_field_string_table cf WHERE cf.field_id = 1 AND cf.bug_id = mantis_bug_table.id) AS 'Cliente' FROM mantis_custom_field_string_table JOIN mantis_bug_relationship_table ON mantis_custom_field_string_table.bug_id = mantis_bug_relationship_table.destination_bug_id JOIN mantis_custom_field_table ON mantis_custom_field_string_table.field_id = mantis_custom_field_table.id JOIN mantis_bug_table ON mantis_bug_table.id = mantis_bug_relationship_table.destination_bug_id JOIN mantis_project_table ON mantis_bug_table.project_id = mantis_project_table.id LEFT JOIN mantis_user_table relator ON relator.id = mantis_bug_table.reporter_id LEFT JOIN mantis_user_table responsavel ON responsavel.id = mantis_bug_table.handler_id WHERE mantis_bug_relationship_table.source_bug_id = ".$mantis_sprint." AND mantis_bug_table.status= 50 GROUP BY Mantis ORDER BY Resumo DESC";

		$query = $this->db->query($sql);

		return $query->result();
	}

	public function revisao($data) { //MANTIS ESTADO REVISAO
	
			$mantis_sprint = $data;

		$sql = "SELECT mantis_project_table.id AS 'Projeto ID', mantis_project_table.name AS 'Projeto Nome', mantis_bug_table.id AS Mantis, mantis_bug_table.reporter_id AS 'Relator ID', relator.username AS 'Relator Usuário', relator.realname AS 'Relator Nome', mantis_bug_table.handler_id AS 'Atribuido a', responsavel.username AS 'Atribuido a Usuário', responsavel.realname AS 'Atribuido a Nome', mantis_bug_table.summary AS 'Resumo', mantis_bug_table.category_id AS 'Categoria', mantis_bug_table.status AS 'Estado', mantis_custom_field_table.name AS 'Campo Nome', mantis_custom_field_string_table.field_id AS 'Campo ID',mantis_custom_field_string_table.value AS 'Campo Conteúdo', (SELECT cf.value FROM mantis_custom_field_string_table cf WHERE cf.field_id = 1 AND cf.bug_id = mantis_bug_table.id) AS 'Cliente' FROM mantis_custom_field_string_table JOIN mantis_bug_relationship_table ON mantis_custom_field_string_table.bug_id = mantis_bug_relationship_table.destination_bug_id JOIN mantis_custom_field_table ON mantis_custom_field_string_table.field_id = mantis_custom_field_table.id JOIN mantis_bug_table ON mantis_bug_table.id = mantis_bug_relationship_table.destination_bug_id JOIN mantis_project_table ON mantis_bug_table.project_id = mantis_project_table.id LEFT JOIN mantis_user_table relator ON relator.id = mantis_bug_table.reporter_id LEFT JOIN mantis_user_table responsavel ON responsavel.id = mantis_bug_table.handler_id WHERE mantis_bug_relationship_table.source_bug_id = ".$mantis_sprint." AND mantis_bug_table.status= 80 GROUP BY Mantis ORDER BY Resumo DESC";

		$query = $this->db->query($sql);

		return $query->result();
	}

	public function refatoracao($data) { //MANTIS ESTADO REFATORACAO
	
		$mantis_sprint = $data;

		$sql = "SELECT mantis_project_table.id AS 'Projeto ID', mantis_project_table.name AS 'Projeto Nome', mantis_bug_table.id AS Mantis, mantis_bug_table.reporter_id AS 'Relator ID', relator.username AS 'Relator Usuário', relator.realname AS 'Relator Nome', mantis_bug_table.handler_id AS 'Atribuido a', responsavel.username AS 'Atribuido a Usuário', responsavel.realname AS 'Atribuido a Nome', mantis_bug_table.summary AS 'Resumo', mantis_bug_table.category_id AS 'Categoria', mantis_bug_table.status AS 'Estado', mantis_custom_field_table.name AS 'Campo Nome', mantis_custom_field_string_table.field_id AS 'Campo ID',mantis_custom_field_string_table.value AS 'Campo Conteúdo', (SELECT cf.value FROM mantis_custom_field_string_table cf WHERE cf.field_id = 1 AND cf.bug_id = mantis_bug_table.id) AS 'Cliente' FROM mantis_custom_field_string_table JOIN mantis_bug_relationship_table ON mantis_custom_field_string_table.bug_id = mantis_bug_relationship_table.destination_bug_id JOIN mantis_custom_field_table ON mantis_custom_field_string_table.field_id = mantis_custom_field_table.id JOIN mantis_bug_table ON mantis_bug_table.id = mantis_bug_relationship_table.destination_bug_id JOIN mantis_project_table ON mantis_bug_table.project_id = mantis_project_table.id LEFT JOIN mantis_user_table relator ON relator.id = mantis_bug_table.reporter_id LEFT JOIN mantis_user_table responsavel ON responsavel.id = mantis_bug_table.handler_id WHERE mantis_bug_relationship_table.source_bug_id = ".$mantis_sprint." AND mantis_bug_table.status= 30 GROUP BY Mantis ORDER BY Resumo DESC";

		$query = $this->db->query($sql);

		return $query->result();
	}

	public function testes($data) //MANTIS ESTADO TESTES
	{
			$mantis_sprint = $data;

		$sql = "SELECT mantis_project_table.id AS 'Projeto ID', mantis_project_table.name AS 'Projeto Nome', mantis_bug_table.id AS Mantis, mantis_bug_table.reporter_id AS 'Relator ID', relator.username AS 'Relator Usuário', relator.realname AS 'Relator Nome', mantis_bug_table.handler_id AS 'Atribuido a', responsavel.username AS 'Atribuido a Usuário', responsavel.realname AS 'Atribuido a Nome', mantis_bug_table.summary AS 'Resumo', mantis_bug_table.category_id AS 'Categoria', mantis_bug_table.status AS 'Estado', mantis_custom_field_table.name AS 'Campo Nome', mantis_custom_field_string_table.field_id AS 'Campo ID',mantis_custom_field_string_table.value AS 'Campo Conteúdo', (SELECT cf.value FROM mantis_custom_field_string_table cf WHERE cf.field_id = 1 AND cf.bug_id = mantis_bug_table.id) AS 'Cliente' FROM mantis_custom_field_string_table JOIN mantis_bug_relationship_table ON mantis_custom_field_string_table.bug_id = mantis_bug_relationship_table.destination_bug_id JOIN mantis_custom_field_table ON mantis_custom_field_string_table.field_id = mantis_custom_field_table.id JOIN mantis_bug_table ON mantis_bug_table.id = mantis_bug_relationship_table.destination_bug_id JOIN mantis_project_table ON mantis_bug_table.project_id = mantis_project_table.id LEFT JOIN mantis_user_table relator ON relator.id = mantis_bug_table.reporter_id LEFT JOIN mantis_user_table responsavel ON responsavel.id = mantis_bug_table.handler_id WHERE mantis_bug_relationship_table.source_bug_id = ".$mantis_sprint." AND mantis_bug_table.status= 70 GROUP BY Mantis ORDER BY Resumo DESC";

		$query = $this->db->query($sql);

		return $query->result();
	}

	public function retornos($data) //MANTIS ESTADO RETORNO
	{
			$mantis_sprint = $data;

		$sql = "SELECT mantis_project_table.id AS 'Projeto ID', mantis_project_table.name AS 'Projeto Nome', mantis_bug_table.id AS Mantis, mantis_bug_table.reporter_id AS 'Relator ID', relator.username AS 'Relator Usuário', relator.realname AS 'Relator Nome', mantis_bug_table.handler_id AS 'Atribuido a', responsavel.username AS 'Atribuido a Usuário', responsavel.realname AS 'Atribuido a Nome', mantis_bug_table.summary AS 'Resumo', mantis_bug_table.category_id AS 'Categoria', mantis_bug_table.status AS 'Estado', mantis_custom_field_table.name AS 'Campo Nome', mantis_custom_field_string_table.field_id AS 'Campo ID',mantis_custom_field_string_table.value AS 'Campo Conteúdo', (SELECT cf.value FROM mantis_custom_field_string_table cf WHERE cf.field_id = 1 AND cf.bug_id = mantis_bug_table.id) AS 'Cliente' FROM mantis_custom_field_string_table JOIN mantis_bug_relationship_table ON mantis_custom_field_string_table.bug_id = mantis_bug_relationship_table.destination_bug_id JOIN mantis_custom_field_table ON mantis_custom_field_string_table.field_id = mantis_custom_field_table.id JOIN mantis_bug_table ON mantis_bug_table.id = mantis_bug_relationship_table.destination_bug_id JOIN mantis_project_table ON mantis_bug_table.project_id = mantis_project_table.id LEFT JOIN mantis_user_table relator ON relator.id = mantis_bug_table.reporter_id LEFT JOIN mantis_user_table responsavel ON responsavel.id = mantis_bug_table.handler_id WHERE mantis_bug_relationship_table.source_bug_id = ".$mantis_sprint." AND mantis_bug_table.status= 20 GROUP BY Mantis ORDER BY Resumo DESC";

		$query = $this->db->query($sql);

		return $query->result();
	}

	public function aprovados($data) //MANTIS ESTADO APROVADO
	{
			$mantis_sprint = $data;

		$sql = "SELECT mantis_project_table.id AS 'Projeto ID', mantis_project_table.name AS 'Projeto Nome', mantis_bug_table.id AS Mantis, mantis_bug_table.reporter_id AS 'Relator ID', relator.username AS 'Relator Usuário', relator.realname AS 'Relator Nome', mantis_bug_table.handler_id AS 'Atribuido a', responsavel.username AS 'Atribuido a Usuário', responsavel.realname AS 'Atribuido a Nome', mantis_bug_table.summary AS 'Resumo', mantis_bug_table.category_id AS 'Categoria', mantis_bug_table.status AS 'Estado', mantis_custom_field_table.name AS 'Campo Nome', mantis_custom_field_string_table.field_id AS 'Campo ID',mantis_custom_field_string_table.value AS 'Campo Conteúdo', (SELECT cf.value FROM mantis_custom_field_string_table cf WHERE cf.field_id = 1 AND cf.bug_id = mantis_bug_table.id) AS 'Cliente' FROM mantis_custom_field_string_table JOIN mantis_bug_relationship_table ON mantis_custom_field_string_table.bug_id = mantis_bug_relationship_table.destination_bug_id JOIN mantis_custom_field_table ON mantis_custom_field_string_table.field_id = mantis_custom_field_table.id JOIN mantis_bug_table ON mantis_bug_table.id = mantis_bug_relationship_table.destination_bug_id JOIN mantis_project_table ON mantis_bug_table.project_id = mantis_project_table.id LEFT JOIN mantis_user_table relator ON relator.id = mantis_bug_table.reporter_id LEFT JOIN mantis_user_table responsavel ON responsavel.id = mantis_bug_table.handler_id WHERE mantis_bug_relationship_table.source_bug_id = ".$mantis_sprint." AND mantis_bug_table.status= 40 GROUP BY Mantis ORDER BY Resumo DESC";

		$query = $this->db->query($sql);

		return $query->result();
	}

	public function fechados($data) //MANTIS ESTADO FECHADO
	{
			$mantis_sprint = $data;

		$sql = "SELECT mantis_project_table.id AS 'Projeto ID', mantis_project_table.name AS 'Projeto Nome', mantis_bug_table.id AS Mantis, mantis_bug_table.reporter_id AS 'Relator ID', relator.username AS 'Relator Usuário', relator.realname AS 'Relator Nome', mantis_bug_table.handler_id AS 'Atribuido a', responsavel.username AS 'Atribuido a Usuário', responsavel.realname AS 'Atribuido a Nome', mantis_bug_table.summary AS 'Resumo', mantis_bug_table.category_id AS 'Categoria', mantis_bug_table.status AS 'Estado', mantis_custom_field_table.name AS 'Campo Nome', mantis_custom_field_string_table.field_id AS 'Campo ID',mantis_custom_field_string_table.value AS 'Campo Conteúdo', (SELECT cf.value FROM mantis_custom_field_string_table cf WHERE cf.field_id = 1 AND cf.bug_id = mantis_bug_table.id) AS 'Cliente' FROM mantis_custom_field_string_table JOIN mantis_bug_relationship_table ON mantis_custom_field_string_table.bug_id = mantis_bug_relationship_table.destination_bug_id JOIN mantis_custom_field_table ON mantis_custom_field_string_table.field_id = mantis_custom_field_table.id JOIN mantis_bug_table ON mantis_bug_table.id = mantis_bug_relationship_table.destination_bug_id JOIN mantis_project_table ON mantis_bug_table.project_id = mantis_project_table.id LEFT JOIN mantis_user_table relator ON relator.id = mantis_bug_table.reporter_id LEFT JOIN mantis_user_table responsavel ON responsavel.id = mantis_bug_table.handler_id WHERE mantis_bug_relationship_table.source_bug_id = ".$mantis_sprint." AND mantis_bug_table.status= 90 GROUP BY Mantis ORDER BY Resumo DESC";

		$query = $this->db->query($sql);

		return $query->result();
	}

	public function carregar_sprints()
	{
			$sql = "SELECT * FROM (SELECT 
					    mantis_bug_table.id,
					    mantis_bug_table.summary,
					    mantis_bug_table.status,
					    mantis_bug_table.date_submitted 
					  FROM
					    mantis_bug_table 
					  WHERE summary REGEXP '^Sprint.[0-9]*$' 
					    AND mantis_bug_table.id NOT IN (4633, 4748, 888, 999) 
					    -- AND YEAR(DATE(FROM_UNIXTIME(date_submitted))) = YEAR(CURTIME())
					  ORDER BY summary DESC)
					 AS TBL 
					UNION
					(SELECT 
					  mantis_bug_table.id,
					  mantis_bug_table.summary,
					  mantis_bug_table.status,
					  mantis_bug_table.date_submitted 
					FROM
					  mantis_bug_table 
					WHERE mantis_bug_table.id IN (888, 999))";

		$query = $this->db->query($sql);

		return $query->result();
	}

	public function status_sprint($mantis_sprint)
	{
		$sql = "SELECT 
				  mantis_bug_table.id,
				  mantis_bug_table.summary,
				  mantis_bug_text_table.description
				FROM
			  	mantis_bug_table
				JOIN mantis_bug_text_table
				ON mantis_bug_table.id = mantis_bug_text_table.id
				WHERE mantis_bug_table.id =".$mantis_sprint.";";

		$query = $this->db->query($sql);

		return $query->result();
	}
	
}
