<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
	
<div class="row">

	<div style="margin-top: 20%; margin-bottom: 15%;" align="center" class="col s12 m6 l8 offset-m6 offset-l4 container hide-on-med-and-up">
			<img data-caption="R. João Suassuna, 51 - Boa Vista, Recife - PE, 50050-350" height="150" width="250" src="<?php echo base_url('materialize/img/header.jpg')?>">
		</div>
		<div style="margin-top: 5%; margin-bottom: 5%;"; align="center" class="container hide-on-small-only">
			<img data-caption="R. João Suassuna, 51 - Boa Vista, Recife - PE, 50050-350" height="150" width="250" src="<?php echo base_url('materialize/img/header.jpg')?>">
		</div>
				
		<form action="Home/index" method="post" accept-charset="utf-8">
			<div class="col s12 m6 l8 offset-m3 offset-l2 container">	
				<select name="mantis_sprint" class="badge center">
				<?php

				 $lista_sprint = json_decode(json_encode($lista_mantis), True); 
				 $verifica = count($lista_sprint);
				 $ultimo = $lista_sprint[ count($lista_sprint) - 3 ];
				 
				for ($i=0; $i <= ($verifica-1); $i++) {
                
                    if ($lista_sprint[$i]['id'] == $ultimo['id']) { $select = "selected"; }else{ $select = ""; }
	echo "<option name='mantis_sprint' value='".$lista_sprint[$i]['id']."' $select >".$lista_sprint[$i]['summary']."</option>";
					}						
								  
				?>
				</select>		
				<span><input class="btn red col m6 offset-m3 s8 offset-s2 l6 offset-l3" type="submit" name="btn-sub" value="Exibir"></span>				
			</div>
										
		</form>	
</div>
