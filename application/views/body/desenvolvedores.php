<?php
defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row center">
  <div class="col s12 m4 offset-m4">
    <div class="white-text black card-panel mantis-atribuido">
      <span class=" mantis-atribuido"><h5>Doing(Atribuídos)</h5>
      </span>
    </div>
  </div>
</div>

<div class="row">
  <div class="col s12 m6">
    <div class="card mantis-atribuido darken-1">
      <div class="card-content ">
        <span class="card-title">Novos</span>
        <p><table class="striped">
         <thead>
          <tr>
            <th data-field="id">Mantis</th>
            <th data-field="name">Descrição</th>
            <th data-field="price">Cliente</th>
          </tr>
        </thead>

        <tbody>
          <tr>
            <td>4572</td>
            <td>OAB</td>
            <td>Atribuido</td>
          </tr>
          <tr>
            <td>4483</td>
            <td>OAB</td>
            <td>Retorno</td>
          </tr>
          <tr>
            <td>5277</td>
            <td>OAB</td>
            <td>Aprovado</td>
          </tr>
        </tbody>
      </table></p>
    </div>
    <div class="card-action">
    </div>
  </div>
</div>

<div class="col s12 m6">
  <div class="card mantis-atribuido darken-1">
    <div class="card-content ">
      <span class="card-title">Atribuidos</span>
      <p><table class="striped">
       <thead>
        <tr>
          <th data-field="id">Mantis</th>
          <th data-field="name">Descrição</th>
          <th data-field="price">Cliente</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>4572</td>
          <td>OAB</td>
          <td>Atribuido</td>
        </tr>
        <tr>
          <td>4483</td>
          <td>OAB</td>
          <td>Retorno</td>
        </tr>
        <tr>
          <td>5277</td>
          <td>OAB</td>
          <td>Aprovado</td>
        </tr>
      </tbody>
    </table></p>
  </div>
  <div class="card-action">
  </div>
</div>
</div>

<div class="col s12 m6">
  <div class="card mantis-atribuido darken-1">
    <div class="card-content ">
      <span class="card-title">Resolvidos</span>
      <p><table class="striped">
       <thead>
        <tr>
          <th data-field="id">Mantis</th>
          <th data-field="name">Descrição</th>
          <th data-field="price">Cliente</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>4572</td>
          <td>OAB</td>
          <td>Atribuido</td>
        </tr>
        <tr>
          <td>4483</td>
          <td>OAB</td>
          <td>Retorno</td>
        </tr>
        <tr>
          <td>5277</td>
          <td>OAB</td>
          <td>Aprovado</td>
        </tr>
      </tbody>
    </table></p>
  </div>
  <div class="card-action">
  </div>
</div>
</div>


<div class="col s12 m6">
  <div class="card mantis-atribuido darken-1">
    <div class="card-content ">
      <span class="card-title">Testes</span>
      <p><table class="striped">
       <thead>
        <tr>
          <th data-field="id">Mantis</th>
          <th data-field="name">Descrição</th>
          <th data-field="price">Cliente</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>4572</td>
          <td>OAB</td>
          <td>Atribuido</td>
        </tr>
        <tr>
          <td>4483</td>
          <td>OAB</td>
          <td>Retorno</td>
        </tr>
        <tr>
          <td>5277</td>
          <td>OAB</td>
          <td>Aprovado</td>
        </tr>
      </tbody>
    </table></p>
  </div>
  <div class="card-action">
  </div>
</div>
</div>


<div class="col s12 m6">
  <div class="card mantis-atribuido darken-1">
    <div class="card-content ">
      <span class="card-title">Retornos</span>
      <p><table class="striped">
       <thead>
        <tr>
          <th data-field="id">Mantis</th>
          <th data-field="name">Descrição</th>
          <th data-field="price">Cliente</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>4572</td>
          <td>OAB</td>
          <td>Atribuido</td>
        </tr>
        <tr>
          <td>4483</td>
          <td>OAB</td>
          <td>Retorno</td>
        </tr>
        <tr>
          <td>5277</td>
          <td>OAB</td>
          <td>Aprovado</td>
        </tr>
      </tbody>
    </table></p>
  </div>
  <div class="card-action">
  </div>
</div>
</div>


<div class="col s12 m6">
  <div class="card mantis-atribuido darken-1">
    <div class="card-content ">
      <span class="card-title">Impedimentos</span>
      <p><table class="striped">
       <thead>
        <tr>
          <th data-field="id">Mantis</th>
          <th data-field="name">Descrição</th>
          <th data-field="price">Cliente</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>4572</td>
          <td>OAB</td>
          <td>Atribuido</td>
        </tr>
        <tr>
          <td>4483</td>
          <td>OAB</td>
          <td>Retorno</td>
        </tr>
        <tr>
          <td>5277</td>
          <td>OAB</td>
          <td>Aprovado</td>
        </tr>
      </tbody>
    </table></p>
  </div>
  <div class="card-action">

  </div>
</div>
</div>
</div>





