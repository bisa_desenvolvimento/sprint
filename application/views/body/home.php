<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row">

  <div id="top" class="col s12 m4 offset-m4 section scrollspy">
    <div class="center white-text card-panel red">
      <span class=""><h5><?php echo $status_sprint[0]['id']." - ".$status_sprint[0]['summary'];?></h5></span>
      <p class=""><?php echo $status_sprint[0]['description']; ?></p>
    </div>
  </div>
  <div class="col s12 m12" style="z-index: 50;">
      <ul class="section table-of-contents">
        <a class="mantis-impedimento btn col s4" href="#impedimentos"><b>Impedimentos</b></a>
        <a class="mantis-retorno btn col s4" href="#retornos"><b>Retornos</b></a>
        <a class="mantis-refatoracao btn col s4" href="#refatoracao"><b>Refatoração</b></a>
        <a class="mantis-novo btn col s4" href="#backlog"><b>Sprint Backlog (Novos)</b></a>
        <a class="mantis-atribuido btn col s4" href="#atribuidos"><b>Atribuidos</b></a>
        <a class="mantis-revisao btn col s4" href="#revisao"><b>Revisão</b></a>
        <a class="mantis-teste btn col s4" href="#testes"><b>Testes</b></a>
        <a class="mantis-aprovado btn col s4 offset-s2" href="#done"><b>Aprovados</b></a>
        <a class="mantis-fechado btn col s4 " href="#fechado"><b>Fechados</b></a>
      </ul>
    </div>
</div>

<div class="row">

<div class="col s12 m12">
  <div id="impedimentos" class="card mantis-impedimento darken-1 section scrollspy">
    <div class="card-content ">
      <span class="card-title">Impedimentos</span>
      <p><table class="bordered">
       <thead>
        <tr>
          <th data-field="" class="coluna-mantis">Mantis</th>
          <th data-field="" class="coluna-resumo">Descrição</th>
          <th data-field="" class="coluna-cliente">Cliente</th>
          <th class="coluna-projeto hide-on-small-only" data-field="">Projeto</th>
          <th class="coluna-atribuido" data-field="">Atribuído</th>
        </tr>
      </thead>
      <tbody>
         <?php


                $lista_impedimentos = json_decode(json_encode($impedimentos), True);

                $verifica = count($lista_impedimentos); 
                    
              if ($verifica == 0){
                echo "<h6>Nenhum mantis cadastrado.</h6>";
              } else {
                  for ($i=0; $i < $verifica; $i++) {
                      echo "<tr>";
                      echo "<td class='coluna-mantis' style='width: 5%;'><a target='_blank' href='http://mantis.bisaweb.com.br/view.php?id=".$lista_impedimentos[$i]['Mantis']."'>".$lista_impedimentos[$i]['Mantis']."</a></td>";
                      echo "<td class='coluna-resumo'>".$lista_impedimentos[$i]['Resumo']."</td>";
                      echo "<td class='coluna-cliente'>".$lista_impedimentos[$i]['Cliente']."</td>";
                      echo "<td class='coluna-projeto hide-on-small-only'>".$lista_impedimentos[$i]['Projeto Nome']."</td>";
                      echo "<td class='coluna-atribuido'>".$lista_impedimentos[$i]['Atribuido a Nome']."</td>";
                      echo "</tr>";
                  } 
   
              }               

          ?>
      </tbody>
    </table></p>
  </div>
  <div class="card-action">
  </div>
</div>
</div>




<div class="col s12 m12">
  <div id="retornos" class="card mantis-retorno darken-1 section scrollspy">
    <div class="card-content ">
      <span class="card-title">Retornos</span>
      <p><table class="bordered">
       <thead>
        <tr>
          <th data-field="" class="coluna-mantis">Mantis</th>
          <th data-field="" class="coluna-resumo">Descrição</th>
          <th data-field="" class="coluna-cliente">Cliente</th>
          <th class="coluna-projeto hide-on-small-only" data-field="">Projeto</th>
          <th class="coluna-atribuido" data-field="">Atribuído</th>
        </tr>
      </thead>
      <tbody>
          <?php 
                $lista_retornos = json_decode(json_encode($retornos), True); 

                  $verifica = count($lista_retornos); 
                    
              if ($verifica == 0){
                echo "<h6>Nenhum mantis cadastrado.</h6>";
              } else {
                
              }

                  for ($i=0; $i < $verifica; $i++) {
                      echo "<tr>";
                      echo "<td class='coluna-mantis'><a target='_blank' href='http://mantis.bisaweb.com.br/view.php?id=".$lista_retornos[$i]['Mantis']."'>".$lista_retornos[$i]['Mantis']."</a></td>";
                      echo "<td class='coluna-resumo'>".$lista_retornos[$i]['Resumo']."</td>";
                      echo "<td class='coluna-cliente'>".$lista_retornos[$i]['Cliente']."</td>";                      
                      echo "<td class='coluna_projeto hide-on-small-only'>".$lista_retornos[$i]['Projeto Nome']."</td>";
                      echo "<td class='coluna-atribuido'>".$lista_retornos[$i]['Atribuido a Nome']."</td>";
                      echo "</tr>";
                  }      
          ?>
      </tbody>
    </table></p>
  </div>
  <div class="card-action">
  </div>
</div>
</div>


<div class="col s12 m12">
  <div id="refatoracao" class="card mantis-refatoracao darken-1 section scrollspy">
    <div class="card-content ">
      <span class="card-title">Refatoração</span>
      <p><table class="bordered">
       <thead>
        <tr>
          <th data-field="" class="coluna-mantis">Mantis</th>
          <th data-field="" class="coluna-resumo">Descrição</th>
          <th data-field="" class="coluna-cliente">Cliente</th>
          <th class="coluna-projeto hide-on-small-only" data-field="">Projeto</th>
          <th class="coluna-atribuido" data-field="">Atribuído</th>
        </tr>
      </thead>
      <tbody>
          <?php 
                $lista_refatoracao = json_decode(json_encode($refatoracao), True); 

                  $verifica = count($lista_refatoracao); 
                    
              if ($verifica == 0){
                echo "<h6>Nenhum mantis cadastrado.</h6>";
              } else {
                
              }

                  for ($i=0; $i < $verifica; $i++) {
                      echo "<tr>";
                      echo "<td class='coluna-mantis'><a target='_blank' href='http://mantis.bisaweb.com.br/view.php?id=".$lista_refatoracao[$i]['Mantis']."'>".$lista_refatoracao[$i]['Mantis']."</a></td>";
                      echo "<td class='coluna-resumo'>".$lista_refatoracao[$i]['Resumo']."</td>";
                      echo "<td class='coluna-cliente'>".$lista_refatoracao[$i]['Cliente']."</td>";                      
                      echo "<td class='coluna_projeto hide-on-small-only'>".$lista_refatoracao[$i]['Projeto Nome']."</td>";
                      echo "<td class='coluna-atribuido'>".$lista_refatoracao[$i]['Atribuido a Nome']."</td>";
                      echo "</tr>";
                  }      
          ?>
      </tbody>
    </table></p>
  </div>
  <div class="card-action">
  </div>
</div>
</div>


  <div class="col s12 m12">
    <div id="backlog" class="card mantis-novo darken-1 section scrollspy">
      <div class="card-content ">
        <span class="card-title">Sprint Backlog (Novos)</span>
        <p><table class="bordered">
         <thead>
          <tr>
            <th data-field="">Mantis</th>
            <th data-field="">Descrição</th>
            <th data-field="">Cliente</th>
            <th class="hide-on-small-only" data-field="">Projeto</th>
            <!-- <th data-field="">Atribuído</th> -->
          </tr>
        </thead>
        <tbody>
          <?php 
                $lista_novos = json_decode(json_encode($novos), True); 

                $verifica = count($lista_novos); 
                      
                if ($verifica == 0){
                  echo "<h6>Nenhum mantis cadastrado.</h6>";
                } else {
                    for ($i=0; $i < $verifica; $i++) {
                      echo "<tr>";
                      echo "<td class='coluna-mantis'><a target='_blank' href='http://mantis.bisaweb.com.br/view.php?id=".$lista_novos[$i]['Mantis']."'>".$lista_novos[$i]['Mantis']."</a></td>";
                      echo "<td clas='coluna-resumo'>".$lista_novos[$i]['Resumo']."</td>";
                      echo "<td class='coluna-cliente'>".$lista_novos[$i]['Cliente']."</td>";
                      echo "<td class='coluna-projeto hide-on-small-only'>".$lista_novos[$i]['Projeto Nome']."</td>";
                      echo "<td class='coluna-atribuido'>".$lista_novos[$i]['Atribuido a Nome']."</td>";
                      echo "</tr>";
                    }
                }
                
          ?>
        </tbody>
      </table></p>
    </div>
    <div class="card-action">
    </div>
  </div>
</div>


<div class="col s12 m12">
  <div id="atribuidos" class="card mantis-atribuido darken-1 section scrollspy">
    <div class="card-content ">
      <span class="card-title">Atribuidos</span>
      <p><table class="bordered">
       <thead>
        <tr>
         <th data-field="" class="coluna-mantis">Mantis</th>
          <th data-field="" class="coluna-resumo">Descrição</th>
          <th data-field="" class="coluna-cliente">Cliente</th>
          <th class="coluna-projeto hide-on-small-only" data-field="">Projeto</th>
          <th class="coluna-atribuido" data-field="">Atribuído</th>
        </tr>
      </thead>
      <tbody>       
      <?php 
                $lista_atribuidos = json_decode(json_encode($atribuidos), True); 

                $verifica = count($lista_atribuidos); 
                      
                if ($verifica == 0){
                  echo "<h6>Nenhum mantis cadastrado.</h6>";
                } else {
                    for ($i=0; $i < $verifica; $i++) {
                    echo "<tr>";
                    echo "<td class='coluna-mantis'><a target='_blank' href='http://mantis.bisaweb.com.br/view.php?id=".$lista_atribuidos[$i]['Mantis']."'>".$lista_atribuidos[$i]['Mantis']."</a></td>";
                    echo "<td class='coluna-resumo'>".$lista_atribuidos[$i]['Resumo']."</td>";
                    echo "<td class='coluna-cliente'>".$lista_atribuidos[$i]['Cliente']."</td>";
                    echo "<td class='coluna-projeto hide-on-small-only'>".$lista_atribuidos[$i]['Projeto Nome']."</td>";
                    echo "<td class='atribuido'>".$lista_atribuidos[$i]['Atribuido a Nome']."</td>";
                    echo "</tr>";
                    }
                }
                      
       ?>      
      </tbody>
    </table></p>
  </div>
  <div class="card-action">
  </div>
</div>
</div>



<div class="col s12 m12">
  <div id="revisao" class="card mantis-revisao darken-1 section scrollspy">
    <div class="card-content ">
      <span class="card-title">Revisão</span>
      <p><table class="bordered">
       <thead>
        <tr>
          <th data-field="" class="coluna-mantis">Mantis</th>
          <th data-field="" class="coluna-resumo">Descrição</th>
          <th data-field="" class="coluna-cliente">Cliente</th>
          <th class="coluna-projeto hide-on-small-only" data-field="">Projeto</th>
          <th class="coluna-atribuido" data-field="">Atribuído</th>
        </tr>
      </thead>
      <tbody>
         <?php 
                $lista_revisao = json_decode(json_encode($revisao), True);

              $verifica = count($lista_revisao); 
                    
              if ($verifica == 0){
                echo "<h6>Nenhum mantis cadastrado.</h6>";
              } else {
                   for ($i=0; $i < $verifica; $i++) {
                    echo "<tr>";
                    echo "<td class='coluna-mantis'><a target='_blank' href='http://mantis.bisaweb.com.br/view.php?id=".$lista_revisao[$i]['Mantis']."'>".$lista_revisao[$i]['Mantis']."</a></td>";
                    echo "<td class='coluna-resumo'>".$lista_revisao[$i]['Resumo']."</td>";
                    echo "<td class='coluna-cliente'>".$lista_revisao[$i]['Cliente']."</td>";
                    echo "<td class='coluna-projeto hide-on-small-only'>".$lista_revisao[$i]['Projeto Nome']."</td>";
                    echo "<td class='atribuido'>".$lista_revisao[$i]['Atribuido a Nome']."</td>";
                    echo "</tr>";
          } 
              }

                    
       ?>      
      </tbody>
    </table></p>
  </div>
  <div class="card-action">
  </div>
</div>
</div>



<div class="col s12 m12">
  <div id="testes" class="card mantis-teste darken-1 section scrollspy">
    <div class="card-content ">
      <span class="card-title">Testes</span>
      <p><table class="bordered">
       <thead>
        <tr>
          <th data-field="" class="coluna-mantis">Mantis</th>
          <th data-field="" class="coluna-resumo">Descrição</th>
          <th data-field="" class="coluna-cliente">Cliente</th>
          <th class="coluna-projeto hide-on-small-only" data-field="">Projeto</th>
          <th class="coluna-atribuido" data-field="">Atribuído</th>
        </tr>
      </thead>
      <tbody>
          <?php 
                $lista_testes = json_decode(json_encode($testes), True);

              $verifica = count($lista_testes); 
                    
              if ($verifica == 0){
                echo "<h6>Nenhum mantis cadastrado.</h6>";
              } else {
                  for ($i=0; $i < $verifica; $i++) {
                      echo "<tr>";
                      echo "<td class='coluna-mantis'><a target='_blank' href='http://mantis.bisaweb.com.br/view.php?id=".$lista_testes[$i]['Mantis']."'>".$lista_testes[$i]['Mantis']."</a></td>";
                      echo "<td class='coluna-resumo'>".$lista_testes[$i]['Resumo']."</td>";
                      echo "<td class='coluna-cliente'>".$lista_testes[$i]['Cliente']."</td>";                   
                      echo "<td class='coluna-projeto hide-on-small-only'>".$lista_testes[$i]['Projeto Nome']."</td>";
                      echo "<td class='coluna-atribuido'>".$lista_testes[$i]['Atribuido a Nome']."</td>";
                      echo "</tr>";
                  } 
              }

                       
          ?>
      </tbody>
    </table></p>
  </div>
  <div class="card-action">
  </div>
</div>
</div>



<div class="col s12 m12">
  <div id="done" class="card mantis-aprovado darken-1 section scrollspy">
    <div class="card-content ">
      <span class="card-title">Done (Aprovado)</span>
      <p><table class="bordered">
       <thead>
        <tr>
          <th data-field="" class="coluna-mantis">Mantis</th>
          <th data-field="" class="coluna-resumo">Descrição</th>
          <th data-field="" class="coluna-cliente">Cliente</th>
          <th class="coluna-projeto hide-on-small-only" data-field="">Projeto</th>
          <th class="coluna-atribuido" data-field="">Atribuído</th>
        </tr>
      </thead>
      <tbody>
            <?php 
                $lista_aprovados = json_decode(json_encode($aprovados), True); 

                  $verifica = count($lista_aprovados); 
                    
              if ($verifica == 0){
                echo "<h6>Nenhum mantis cadastrado.</h6>";
              } else {
               for ($i=0; $i < $verifica; $i++) {
                      echo "<tr>";
                      echo "<td class='coluna-mantis'><a target='_blank' href='http://mantis.bisaweb.com.br/view.php?id=".$lista_aprovados[$i]['Mantis']."'>".$lista_aprovados[$i]['Mantis']."</a></td>";
                      echo "<td class='coluna-resumo'>".$lista_aprovados[$i]['Resumo']."</td>";
                      echo "<td class='coluna-cliente'>".$lista_aprovados[$i]['Cliente']."</td>";
                      echo "<td class='coluna-projeto hide-on-small-only'>".$lista_aprovados[$i]['Projeto Nome']."</td>";
                      echo "<td class='coluna-atribuido'>".$lista_aprovados[$i]['Atribuido a Nome']."</td>";
                      echo "</tr>";
                  } 
              }

                       
          ?>
      </tbody>
    </table></p>
  </div>
  <div class="card-action">
  </div>
</div>
</div>


<div class="col s12 m12">
  <div id="fechado" class="card mantis-fechado darken-1 section scrollspy">
    <div class="card-content ">
      <span class="card-title">Fechado</span>
      <p><table class="bordered">
       <thead>
        <tr>
          <th data-field="" class="coluna-mantis">Mantis</th>
          <th data-field="" class="coluna-resumo">Descrição</th>
          <th data-field="" class="coluna-cliente">Cliente</th>
          <th class="coluna-projeto hide-on-small-only" data-field="">Projeto</th>
          <th class="coluna-atribuido" data-field="">Atribuído</th>
        </tr>
      </thead>
      <tbody>
      <?php 
                $lista_fechados = json_decode(json_encode($fechados), True);  

              $verifica = count($lista_fechados); 
                    
              if ($verifica == 0){
                echo "<h6>Nenhum mantis cadastrado.</h6>";
              } else {
                for ($i=0; $i < $verifica; $i++) {
                      echo "<tr>";
                      echo "<td class='coluna-mantis'><a target='_blank' href='http://mantis.bisaweb.com.br/view.php?id=".$lista_fechados[$i]['Mantis']."'>".$lista_fechados[$i]['Mantis']."</a></td>";
                      echo "<td coluna-resumo>".$lista_fechados[$i]['Resumo']."</td>";
                      echo "<td class='coluna-cliente'>".$lista_fechados[$i]['Cliente']."</td>";
                      echo "<td class='coluna-projeto hide-on-small-only'>".$lista_fechados[$i]['Projeto Nome']."</td>";
                      echo "<td class='coluna-atribuido'>".$lista_fechados[$i]['Atribuido a Nome']."</td>";
                      echo "</tr>";
                  }       
              }

                  
          ?>
      </tbody>
    </table></p>
  </div>
  <div class="card-action">
  </div>
</div>
</div>



 <div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
    <a href="#top" class="btn-floating btn-large red">
      <i class="large material-icons">navigation</i>
    </a>
  </div>

</div>





