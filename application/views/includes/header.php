<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <head>
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href=""  media="screen,projection"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="theme-color" content="#000000">
  <link href="<?php echo base_url('materialize/css/materialize.css'); ?>" rel="stylesheet" />
    <link href="<?php echo base_url('materialize/css/style.css'); ?>" rel="stylesheet" />
    <title>::- Sprint - Bisaweb -::</title>   
  </head>
  <body class="">

  
    <nav>
      <div class="black nav-wrapper">
        <a class="brand-logo"><h3><img class="circle white z-depth-3" width="100" height="50"src="<?php echo base_url('materialize/img/Logo.png'); ?>" alt=""></h3></a>
        <a href="#" data-activates="mobile-demo" class="button-collapse <?php echo $estado_menu; ?>" id="nav-btn"><i class="material-icons">menu</i></a>
        <ul class="right hide-on-med-and-down">
          <li><a class="" href="/"><i class="material-icons <?php echo $estado_menu; ?>">fast_rewind</i></a></li>
        </ul>
        <ul class="side-nav" id="mobile-demo">
         <li><a class="" href="/"><i class="material-icons <?php echo $estado_menu; ?>">fast_rewind</i></a></li>
      </ul>
      </div>
    </nav>
