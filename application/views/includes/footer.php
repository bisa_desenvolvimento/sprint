<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="<?php echo base_url('materialize/js/materialize.js'); ?>"></script>
    <script src="<?php echo base_url('materialize/js/init.js'); ?>"></script>

  </body>
</html>