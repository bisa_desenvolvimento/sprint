<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index(){

		$mantis_sprint = $this->input->post('mantis_sprint');

		if (($mantis_sprint) == ''){

			$dados = array('page' => 'default',
				           'estado_menu' => '');
			$this->load->view('body/index', $dados);

		} else {

			$this->load->model('Mhome','',TRUE);
			$lista_impedimentos = $this->Mhome->impedimentos($mantis_sprint);
			$lista_novos = $this->Mhome->sprint($mantis_sprint);
			$lista_atribuidos = $this->Mhome->atribuidos($mantis_sprint);
			$lista_revisao = $this->Mhome->revisao($mantis_sprint);
			$lista_testes = $this->Mhome->testes($mantis_sprint);
			$lista_retorno = $this->Mhome->retornos($mantis_sprint);
			$lista_refatoracao = $this->Mhome->refatoracao($mantis_sprint);
			$lista_aprovados = $this->Mhome->aprovados($mantis_sprint);
			$lista_fechados = $this->Mhome->fechados($mantis_sprint);
			$dados_sprint = $this->Mhome->status_sprint($mantis_sprint);
			$dados_sprint = json_decode(json_encode($dados_sprint), True);

			$dados = array('page' => 'home',
					   'estado_menu' => '',
					   'side_active' => 'active',
					   'impedimentos' => $lista_impedimentos,
					   'novos' => $lista_novos,
					   'atribuidos' => $lista_atribuidos,
					   'revisao' => $lista_revisao,
					   'refatoracao' => $lista_refatoracao,
					   'testes' => $lista_testes,
					   'retornos' => $lista_retorno,
					   'aprovados' => $lista_aprovados,
					   'fechados' => $lista_fechados,
					   'status_sprint' => $dados_sprint 
					);


			$this->load->view('body/index', $dados);
		}
	}

	public function selecao() {
		$this->load->model('Mhome');
		$lista_sprint = $this->Mhome->carregar_sprints();

		$dados = array('page' => 'selecao',
					   'estado_menu' => 'hide',
					   'lista_mantis' => $lista_sprint);
		$this->load->view('body/index', $dados);
	}

}
